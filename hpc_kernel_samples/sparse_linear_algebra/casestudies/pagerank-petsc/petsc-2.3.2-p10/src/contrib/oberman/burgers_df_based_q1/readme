Contents of this directory:

Solves the 2d Burgers equation:
  u*du/dx + v*du/dy - c(lap(u)) = f
  u*dv/dx + v*dv/dy - c(lap(v)) = g.
This has exact solution, see Fletcher.
This version has new indexing of Degrees of Freedom.
The solution technique is by the finite element method, using bilinear elements.

Usage: ?
    generate the grid using input
    solve the equation using main
    view the solution in matlab using bscript.m

example of use:
    call
        main -f pgr44 -options

Grid Generator: ?
        input
        compile with make input
        run as:
        input -xintervals 9 -yintervals 9 -xmin 0.0 -xmax 1.0 -ymin 0.0 -ymax 1.0

Matlab Script: ?
        bscript.m
          -matlab script for viewing the solution.
          Usage:
                Run matlab in the same directory as the solver,
                type: bscript

Nonlinear Solver:
  Usage:
        run in parallel using
        mpirun -np 2 main -f pgr44
                                       
   Contents
        appctx.h
          -define data structure

	main.c
	  -main part of the code.

        functions.c
          - function definitions for the boundary conditions and 
            right hand side of the pde.

	appload.c
	  - get the information from the grid file using database calls.  
            Sets up the local data.

        appsetalg.c
          - setup the nonlinear system and solve it.

	appview.c
	  -utility for viewing the solution.  
           Connects to matlab using the matlab script, bscript.m


The basic philosophy of the codes is to make data-structures and computations cell-oriented.
The boundary conditions are one exception, and the flavor of the code which for applying boundary conditions is consequently very different.  For clarity, the boundary code is kept separate from the rest of the code.

full options:
   
    -matlabgraphics: pipe solution to matlab (visualize with bscript).
    -show_griddata:  print the local index sets and local to global mappings 
    -show_solution:  plot the solution with a contour graph 
    -show_matrix:    visualize the sparsity structure of the stiffness matrix.
    -show_grid:      visualize the global and local grids with numbering.

    -help   for full description of the run-time options available.

