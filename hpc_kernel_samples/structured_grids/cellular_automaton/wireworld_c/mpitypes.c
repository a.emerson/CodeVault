#include <stdlib.h>

#include "mpitypes.h"

#include "configuration.h"

void mpitype_conf_init(MPI_Datatype *new_type)
{
   conf_t dummy;
   int i;

   // CONF_T_* constants defined in configuration.h
   int blocklengths[] = {CONF_T_N_INT_MEMBERS, CONF_T_N_LONG_MEMBERS, CONF_T_N_CHAR_MEMBERS};
   MPI_Datatype types[] = {MPI_INT, MPI_LONG, MPI_CHAR};
   MPI_Aint displacements[3];
   MPI_Aint base;

   MPI_Get_address(&dummy, &base);
   MPI_Get_address(&dummy.CONF_T_FIRST_INT_MEMBER, &displacements[0]);
   MPI_Get_address(&dummy.CONF_T_FIRST_LONG_MEMBER, &displacements[1]);
   MPI_Get_address(&dummy.CONF_T_FIRST_CHAR_MEMBER, &displacements[2]);

   for(i = 0; i < 3; i++) displacements[i] -= base;

   MPI_Type_create_struct(3, blocklengths, displacements, types, new_type);
   MPI_Type_commit(new_type);
}

void mpitype_conf_free(MPI_Datatype *type)
{
   MPI_Type_free(type);
}

