#include <iostream>
#include <string>

#include "Simulation.hpp"

namespace nbody {
	std::vector<Body> Simulation::getBodies() const {
		return bodies;
	}

	bool Simulation::readInputData(const std::string& filename) {
		if (getProcessId() != 0) return true;

		bodies = dubinskiParse(filename);
		return !bodies.empty();
	}
} // namespace nbody
