#include <iostream>
#include <pthread.h>
#include <BarnesHutTree.hpp>
#include <PthreadSimulation.hpp>

using namespace nbody;
using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		cerr << "need input file parameter" << endl;
		return -1;
	}
	PthreadSimulation* simulation = new PthreadSimulation(string(argv[1]));

	simulation->setIterations(6);
	simulation->run();
	delete(simulation);
	return 0;
}
