# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>
#
# ==================================================================================================

cmake_minimum_required (VERSION 3.0 FATAL_ERROR)
file( GLOB simulation_SOURCES *.hpp *.cpp )
include_directories(../datastructures)
add_library( simulation ${simulation_SOURCES} )
target_link_libraries(simulation datastructures ${CMAKE_THREAD_LIBS_INIT})